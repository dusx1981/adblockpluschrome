# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2006-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

---

default:
  image: "registry.gitlab.com/eyeo/docker/adblockplus-ci:node10"

stages:
  - "prepare"
  - "build"
  - "download"
  - "publish"
  - "test"
  - "scp"

variables:
  npm_config_audit: "false"
  npm_config_prefer_offline: "true"
  npm_config_unsafe_perm: "true"

.dependencies:
  script: &dependencies
    - "mkdir -p .git/info"
    - "python ensure_dependencies.py"
    - "npm install"

.cache:
  cache: &cache
    key: "global-cache"
    untracked: true
    paths:
      - "adblockpluscore/"
      - "adblockplusui/"
      - "buildtools/"
      # npm audit fails if the node_modules folder is cached
      # but the changes in package-lock.json are not.
      # That is a bug (https://github.com/npm/npm/issues/20604)
      # which in our case is triggerred by using stubs in package.json.
      - "package-lock.json"

.default_template:
  needs:
    - job: "prepare-dependencies"
  before_script: *dependencies
  retry:
    max: 2
    when: "stuck_or_timeout_failure"
  except:
    - "schedules"
  cache:
    <<: *cache
    policy: "pull"
  interruptible: true

prepare-dependencies:
  stage: "prepare"
  script:
    - "mv -t /tmp adblockpluscore/*-snapshots 2>/dev/null || true"
    - "git clean -x -d -ff"
    - *dependencies
    - "mv -t adblockpluscore /tmp/*-snapshots 2>/dev/null || true"
    - "npm run download-test-browsers"
  except:
    - "schedules"
  cache: *cache
  interruptible: true

###############################################################################
#                               Build & document                              #
###############################################################################

.build:
  extends: ".default_template"
  stage: "build"
  script:
    - "python build.py build -t $PLATFORM -b `npm run -s generate-buildnum`"
    - "python build.py devenv -t $PLATFORM"

build:chrome:
  extends: ".build"
  variables:
    PLATFORM: "chrome"
  artifacts:
    paths:
      - "adblockpluschrome-*.zip"
      - "devenv.chrome/qunit/"

build:gecko:
  extends: ".build"
  variables:
    PLATFORM: "gecko"
  artifacts:
    paths:
      - "adblockplusfirefox-*.xpi"
      - "devenv.gecko/qunit/"

docs:
  extends: ".default_template"
  stage: "build"
  script:
    - "npm run docs"
  artifacts:
    paths:
      - "docs/"
    expire_in: "1 week"

###############################################################################
#                                Tests & checks                               #
###############################################################################

.test_template:
  extends: ".default_template"
  stage: "test"
  before_script:
    - "unzip -q $EXTENSION_FILE -d devenv.*"
    - *dependencies
  variables:
    SKIP_BUILD: "true"
  artifacts:
    paths:
      - "test/screenshots/"
    when: "on_failure"
    expire_in: "1 mo"

.test_template_chromium:
  extends: ".test_template"
  needs:
    - job: "build:chrome"
      artifacts: true
  variables:
    EXTENSION_FILE: "adblockpluschrome-*.zip"

.test_template_firefox:
  extends: ".test_template"
  needs:
    - job: "build:gecko"
      artifacts: true
  variables:
    EXTENSION_FILE: "adblockplusfirefox-*.xpi"

.test_template_edge:
  extends: ".test_template_chromium"
  before_script:
    - "Expand-Archive -Path $EXTENSION_FILE -DestinationPath devenv.chrome"
    - "choco install -y microsoft-edge --version=79.0.309.71"
    - "npm install"
  tags:
    - shared-windows
    - windows
    - windows-1809
  # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/25980
  cache: null

lint:yaml:
  image: "registry.gitlab.com/eyeo/docker/yamllint:1.14.0-configured"
  stage: "test"
  needs: []
  script:
    - "yamllint .gitlab-ci.yml"
  except:
    - "schedules"
  interruptible: true

lint:js:
  extends: ".default_template"
  stage: "test"
  script:
    - "npm run lint"

audit:
  extends: ".default_template"
  stage: "test"
  script:
    - "npm audit"
  allow_failure: true

test:firefox:oldest:
  extends: ".test_template_firefox"
  script:
    - "npm run test-only -- -g 'Firefox \\(oldest\\)'"

test:firefox:latest:
  extends: ".test_template_firefox"
  script:
    - "npm run test-only -- -g 'Firefox \\(latest\\)'"

test:chromium:oldest:
  extends: ".test_template_chromium"
  script:
    - "xvfb-run -a npm run test-only -- -g 'Chromium \\(oldest\\)'"

test:chromium:latest:
  extends: ".test_template_chromium"
  script:
    - "xvfb-run -a npm run test-only -- -g 'Chromium \\(latest\\)'"

test:edge:
  extends: ".test_template_edge"
  script:
    - "npm run test-only -- -g 'Edge'"

###############################################################################
#                                Scheduled jobs                               #
###############################################################################

.download:
  stage: "download"
  script:
    - "npm install"
    - "npm run archive -- $EXTENSION_ID"
  only:
    - "schedules"
  artifacts:
    paths:
      - "adblockpluschrome-*.crx"

.scp:
  stage: "scp"
  script:
    - "scp *.crx $DESTINATION"
  only:
    - "schedules"
  tags:
    - "protected"

download:release:
  extends: ".download"
  variables:
    EXTENSION_ID: "cfhdojbkjhnklbpkdaibdccddilifddb"

download:devbuild:
  extends: ".download"
  variables:
    EXTENSION_ID: "ldcecbkkoecffmfljeihcmifjjdoepkn"

scp:release:
  extends: ".scp"
  needs:
    - job: "download:release"
      artifacts: true
  variables:
    DESTINATION: "builds_user@eyeofiles.com:/var/adblockplus/fileserver/builds/releases/"

scp:devbuild:
  extends: ".scp"
  needs:
    - job: "download:devbuild"
      artifacts: true
  variables:
    DESTINATION: "builds_user@eyeofiles.com:/var/adblockplus/fileserver/builds/devbuilds/adblockpluschrome/"

###############################################################################
#                                 Public pages                                #
###############################################################################

.pages:
  stage: "publish"
  needs:
    - job: "docs"
      artifacts: true
  except:
    - "schedules"

include:
  project: "eyeo/adblockplus/adblockpluscore"
  file: ".gitlab-pages.yml"
